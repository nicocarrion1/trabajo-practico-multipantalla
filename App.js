/**
 * @format
 */
import React from 'react';

import AlbumList from './src/components/AlbumList';
import PhotoList from './src/components/PhotoList';
import {Router, Scene, Stack} from 'react-native-router-flux';
import Comments from './src/components/Comment'

// Create a component
const App = () => (
  <Router>
    <Stack key="root">
      <Scene
        key="albumList"
        component={AlbumList}
        title="Albums"
        initial={true}
      />
      <Scene key="photoList" component={PhotoList} title="Photos" />
      <Scene key="comment" component={Comments} title="Comments" />

    </Stack>
  </Router>
);

export default App;

