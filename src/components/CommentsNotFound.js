import React from "react";
import { Text, View, Linking } from "react-native";
import Card from "./Card";
import CardSection from "./CardSection";

const CommentsNotFound = (photo_id) => {
  return (
    <View>
      <Card>
        <CardSection>
          <Text>No hay comentarios, por favor ingrese a </Text>
          <Text
            style={{ color: "blue" }}
            onPress={() => Linking.openURL(`https://www.flickr.com/photos/188605165@N03/${photo_id.photo_id}/`)}
          >
            Flickr.com
          </Text>
        </CardSection>
      </Card>
    </View>
  );
};

export default CommentsNotFound;
