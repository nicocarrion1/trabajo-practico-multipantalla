import React, { useState, useEffect, Fragment } from "react";
import axios from "axios";
import CommentDetail from './CommentsDetails'
import ComentsNotFound from './CommentsNotFound'
import CommentsNotFound from "./CommentsNotFound";

function Comments({ photo_id }) {
  const [comments, setComments] = useState([]);

  useEffect(() => {
    getComments(photo_id);
  }, []);

  const getComments = async photo_id => {
    try {
      const response = await axios.get(
        `https://api.flickr.com/services/rest/?method=flickr.photos.comments.getList&api_key=720d9494162bc97e69f90c562b62af16&photoset_id=72157714456698777&user_id=188605165@N03&format=json&nojsoncallback=1&photo_id=${photo_id}`
      );
      const comments = response.data.comments.comment;
      console.log(response.data);
      if (comments) {
        setComments(comments)
      }
    } catch (err) {
      console.log(err + "No se encontro la foto");
    }
  };




  const renderComments=()=>{
  
    if(comments.length>0) { return comments.map(comment => <CommentDetail key={comment.id} commentContent={comment._content} autor={comment.realname}  />);
  }
  else {return <CommentsNotFound photo_id={photo_id}/>}
  }

  return (
    <Fragment>
      {renderComments()}
    </Fragment>
  );
}

const styles = {
  headerContentStyle: {
    flexDirection: "column",
    justifyContent: "space-around"
  },
  headerTextStyle: {
    fontSize: 18
  },
  thumbnailStyle: {
    height: 50,
    width: 50
  },
  thumbnailContainerStyle: {
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
    marginRight: 10,
    fontFamily: "Roboto",
    border: "3px solid black",
    marginTop: 20,
    marginBottom: 20,
    padding: 25,
    height: 50
  },
  commentStyle: {
    alignSelf: 'center',
    color: '#007aff',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  },

  imageStyle: {
    height: 300,
    flex: 1,
    width: null
  },
  buttonStyle: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#fff',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#007aff',
    marginLeft: 5,
    marginRight: 5
  },
  containerStyle: {
    borderBottomWidth: 1,
    padding: 5,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    position: 'relative'
  }
};

export default Comments;
