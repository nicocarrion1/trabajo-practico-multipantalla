import React from "react";
import { Button } from "react-native-paper";
import { View } from "react-native";

const PhotoMenu = props => {
  



  return (   
     <View>
         <Button icon ='sort-alphabetical' color="blue" 
         onPress={props.orderByName}>Orden alfabetico
         </Button>
         <Button icon = 'calendar' color="blue" onPress={props.orderByDate}>Orden por fecha</Button>
     </View>
  );
};

export default PhotoMenu;

