import React, { useState, useEffect, Fragment } from "react";
import { ScrollView, View } from "react-native";
import axios from "axios";
import PhotoDetail from "./PhotoDetail";
import Card from "./Card";
import Button from "./Button";
import { Actions } from "react-native-router-flux";
import { ActivityIndicator, Colors } from "react-native-paper";
import PhotoMenu from "./PhotoMenu";
import { API_KEY, USER_ID } from "./keys";

function PhotoList({ albumId }) {
  const [photos, setPhotos] = useState([]);
  const [button, setButton] = useState(null);

  useEffect(() => {
    const extraer_data = async () => {
      try {
        console.log(API_KEY);
        const response = await axios.get(
          `https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=${API_KEY}&photoset_id=${albumId}&user_id=${USER_ID}&format=json&nojsoncallback=1`
        );
        setPhotos(response.data.photoset.photo);
        console.log(response.data.photoset.photo);
      } catch (err) {
        console.log(err);
      }
    };
    extraer_data();
  }, []);

  const sortPhotosName = () => {
    if (button) {
      setButton(false);
      const sortedPhotos = photos.sort((a, b) => {
        if (a.title.toLowerCase() < b.title.toLowerCase()) return -1;
        if (a.title.toLowerCase() > b.title.toLowerCase()) return 1;
        return 0;
      });

      console.log(sortedPhotos);
      setPhotos([...sortedPhotos]);
    } else {
      setButton(true);
      const sortedPhotos = photos.sort((a, b) => {
        if (a.title.toLowerCase() > b.title.toLowerCase()) return -1;
        if (a.title.toLowerCase() < b.title.toLowerCase()) return 1;
        return 0;
      });

      console.log(sortedPhotos);
      setPhotos([...sortedPhotos]);
    }
  };

  const sortPhotosDate = () => {
    if (button) {
      setButton(false);
      const sortedPhotos = photos.sort((a, b) => {
        if (a.id < b.id) return -1;
        if (a.id > b.id) return 1;
        return 0;
      });
      setPhotos([...sortedPhotos]);
    } else {
      setButton(true);
      const sortedPhotos = photos.sort((a, b) => {
        if (a.id > b.id) return -1;
        if (a.id < b.id) return 1;
        return 0;
      });
      setPhotos([...sortedPhotos]);
    }
  };

  const renderAlbums = () => {
    return photos.map(photo => (
      <Fragment key={photo.id}>
        <PhotoDetail
          title={photo.title}
          imageUrl={`https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}.jpg`}
        />
        <Card>
          <View>
            <Button onPress={() => Actions.comment({ photo_id: photo.id })}>
              Comentarios
            </Button>
          </View>
        </Card>

        {/* <Comments photo_id={photo.id}></Comments> */}
      </Fragment>
    ));
  };

  if (!photos) {
    return (
      <View style={{ flex: 1 }}>
        <ActivityIndicator animating={true} size={"large"}></ActivityIndicator>
      </View>
    );
  }

  return (
    <View style={{ flex: 1 }}>
      <PhotoMenu orderByName={sortPhotosName} orderByDate={sortPhotosDate} />
      <ScrollView>{renderAlbums()}</ScrollView>
    </View>
  );
}

export default PhotoList;
